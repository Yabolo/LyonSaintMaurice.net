# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from LyonSaintMaurice_net.EmailSending import SendMail
#import django_select2.urls
from django.contrib.sitemaps.views import sitemap
import photologue.urls
import cms.urls
from django.views.static import serve
from django.views.defaults import server_error, page_not_found #For displaying 404 and 500 errors

from machina.app import board

from django.contrib.auth.views import login, logout
from django.contrib.auth import urls as AuthUrls

import password_reset.urls
# ZINNIA BLOG
import zinnia.urls
import django_comments.urls


admin.autodiscover()

from django.http import HttpResponseRedirect
def custom_login(request, template_name):
	if request.GET:  
		NextURL = request.GET['next']
	if request.user.is_authenticated():
		return HttpResponseRedirect(NextURL)
	else:
		return login(request, template_name)

urlpatterns = [
# ERROR PAGES
    url(r'^500/$', server_error),
    url(r'^404/$', page_not_found),
# -----------
    url(r'^', include(password_reset.urls)),
    url(r'^newsletter/', include('newsletter.urls')),
    url(r'^login/$', custom_login, {'template_name': 'login.html'}),
    url(r'^fr/login/$', custom_login, {'template_name': 'login.html'}),
    url(r'^logout/$', logout, {'next_page': '/'}),
    url('^accounts/', include(AuthUrls)),
    url(r'^envoicourriel/$', SendMail),
    url(r'^forum/', include(board.urls)),
# ZINNIA BLOG
    url(r'^archives/', include(zinnia.urls)),
    url(r'^comments/', include(django_comments.urls)),
# -----------
    url(r'^admin/', include(admin.site.urls)),  # NOQA
    url(r'^', include(cms.urls)),
    url(r'^ressources/photos/', include(photologue.urls, namespace='photologue')),
    url(r'^sitemap\.xml$', sitemap,
        {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^dynamic_forms/',
        include('dynamic_forms.urls', namespace='dynamic_forms')),
#    url(r'^favicon\.ico$', django.views.generic.simple.redirect_to, {'url': '/static/favicon.ico'}),
]

#urlpatterns += i18n_patterns(
#    url(r'^admin/', include(admin.site.urls)),  # NOQA
#    url(r'^select2/', django_select2.urls),
#    url(r'^', include(spirit.urls)),
#    url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')), # For djangocms_blog
#)

# This is only needed when using runserver.
if settings.DEBUG:
	urlpatterns = i18n_patterns(
            url(r'^media/(?P<path>.*)$', serve,
                {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
            ) + staticfiles_urlpatterns() + urlpatterns







#from django.utils.functional import curry
#from django.views.defaults import *
#handler500 = curry(server_error, template_name='500')
#handler404 = curry(page_not_found, template_name='404')
#handler403 = curry(permission_denied, template_name='403')
