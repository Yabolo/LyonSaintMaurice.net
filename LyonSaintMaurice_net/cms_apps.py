#!/usr/bin/python
#-*- coding: utf-8 -*-

from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

#==================================================================

class PhotologueApphook(CMSApp):
	app_name = "photologue-cmshook"
	name = _("Photologue CMS-Hook")
	urls = ["photologue.urls"]

	def get_urls(self, page=None, language=None, **kwargs):
		return ["photologue.urls"]

apphook_pool.register(PhotologueApphook)  # register the application


#==================================================================
from machina.app import board

class MachinaApphook(CMSApp):
	app_name = "machina-cmshook"
	name = _("Machina CMS-Hook")
	urls = ["board.urls"]

	def get_urls(self, page=None, language=None, **kwargs):
		return ["board.urls"]

apphook_pool.register(MachinaApphook)  # register the application

#==================================================================
import zinnia.urls

class ZinniaApphook(CMSApp):
	app_name = "zinnia-cmshook"
	name = _("Zinnia CMS-Hook")
	urls = ["zinnia.urls"]

	def get_urls(self, page=None, language=None, **kwargs):
		return ["zinnia.urls"]

apphook_pool.register(ZinniaApphook)  # register the application

#==================================================================
