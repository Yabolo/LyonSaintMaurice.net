#!/usr/bin/python
#-*- coding: utf-8 -*-


#from django.db import models
from django import forms

from django.views.generic.base import TemplateView
from django.http import HttpResponseRedirect
from django.core.mail import send_mail, BadHeaderError

import requests
from django.conf import settings

#=========================================================================
class EmailForm(forms.Form):
	Nom = forms.CharField(max_length=255)
	Courriel = forms.EmailField()
	Objet = forms.CharField(max_length=255)
	Message = forms.CharField()

#=========================================================================
def SendMail(request):
	if request.method == 'POST':
		ReCaptcha = request.POST.get('g-recaptcha-response', '')
		Success, ErrorCode = CheckReCaptcha(request, ReCaptcha)
		if Success:
			form = EmailForm(request.POST)
			if form.is_valid():
				Nom       = form.cleaned_data['Nom']
				if Nom == "" or Nom is None: Nom="Inconnnu"
				Courriel  = form.cleaned_data['Courriel']
				if Courriel == "" or Courriel is None: Courriel="paroisse@saintmauricelyon.net"
				Objet     = "[SaintMauriceLyon.net] "+form.cleaned_data['Objet']
				Message   = form.cleaned_data['Message']
				try:
					AdresseComplete = Nom + " " + "<" + Courriel + ">"
					Msg="[MESSAGE ENVOYÉ DEPUIS LE SITE INTERNET SAINTMAURICELYON.NET]\n\n"+Message
					send_mail(Objet, Msg, AdresseComplete, ['paroisse@saintmauricelyon.net', 'matthieu.payet4@gmail.com'])
					return HttpResponseRedirect('/courriel-envoye/')
				except:
					return HttpResponseRedirect('/courriel-non-envoye/')
			else:
				return HttpResponseRedirect('/notre-paroisse/contact/')
		else:
			return HttpResponseRedirect('/notre-paroisse/contact/')
	else:
		return HttpResponseRedirect('/notre-paroisse/contact/')

#=========================================================================
def CheckReCaptcha(request, ReCaptcha):
	response = {}
	data = request.POST
	URL = "https://www.google.com/recaptcha/api/siteverify"
	params = {
	    'secret': settings.NORECAPTCHA_SECRET_KEY,
	    'response': ReCaptcha,
	    'remoteip': GetClientIP(request)
	}
	verify_rs = requests.get(URL, params=params, verify=True)
	verify_rs = verify_rs.json()
#	"success": true|false,
#	"challenge_ts": timestamp,  // timestamp of the challenge load (ISO format yyyy-MM-dd'T'HH:mm:ssZZ)
#	"hostname": string,         // the hostname of the site where the reCAPTCHA was solved
#	"error-codes": [...]        // optional

	#missing-input-secret   : The secret parameter is missing.
	#invalid-input-secret   : The secret parameter is invalid or malformed.
	#missing-input-response : The response parameter is missing.
	#invalid-input-response : The response parameter is invalid or malformed.

	Success   = verify_rs.get("success", False)
	ErrorCode = verify_rs.get('error-codes', None) or "Unspecified error."
	TimeStamp = verify_rs.get("challenge_ts", False)
	Hostname  = verify_rs.get("hostname", False)
	print("Check ReCaptcha\n\tSuccess:", Success, "\n\tError code:", ErrorCode, "\n\tHostname:", Hostname, "\n\tTimeStamp:", TimeStamp)
	return Success, ErrorCode


#=========================================================================
def GetClientIP(request):
	x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
	if x_forwarded_for:
		ip = x_forwarded_for.split(',')[0]
	else:
		ip = request.META.get('REMOTE_ADDR')
	return ip
	

## Add to the top
#from collection.forms import ContactForm

## Add to your views
#def Contact(request):
#	form_class = ContactForm

#	return render(request, 'contact.html', {'form': form_class, })




