#!/usr/bin/python
#-*- coding: utf-8 -*-


import datetime, locale
import requests

def SaintDuJour(request):
	locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')
	Date = datetime.datetime.now().strftime("%d %B %Y") #%A (jour)
	try:
		f = requests.get("http://feed.evangelizo.org/v2/reader.php?type=saint&lang=FR")
		Saint = f.text
	except:
		Saint="La Paix soit avec vous !"
	return {'SaintDuJour': Saint, 'Date': Date}









